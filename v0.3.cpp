#include<bits/stdc++.h>
using namespace std;
void CharCount(FILE *file)
{
    int zifu=0;
    char ch;
    ch=fgetc(file);
    while(!feof(file))
    {
        zifu=zifu+1;
        ch=fgetc(file);
    }
    cout<<"字符数:"<<zifu<<endl;
    rewind(file);
}

void WordCount(FILE *file)
{
    int danci=0;
    char ch;
    ch=fgetc(file);
    while(!feof(file))
    {
        if(isalnum(ch))
        {
            while(isalnum(ch)||ch=='-')
            {
                ch=fgetc(file);
            }
            danci=danci+1;
        }

        ch=fgetc(file);
    }
    cout<<"单词数:"<<danci<<endl;
    rewind(file);
}

void LineCount(FILE *file)
{
    int juzi=0;
    char ch;
    ch=fgetc(file);
    while(!feof(file))
    {
        if(ch=='.'||ch=='!'||ch=='?')
        {
            juzi=juzi+1;
        }
        ch=fgetc(file);    
    }
    cout<<"句子数:"<<juzi<<endl;
    rewind(file);
}

void Advance(FILE *file)
{
    int daima=0;
    int konghang=0;
    int zhushihang=0;
    int i=0;
    int hangshu=0;
    char ch,str[100];

    ch=fgetc(file);
    while(!feof(file))
    {
        if(ch=='\n'||ch=='\0')
        {
            hangshu=hangshu+1;
        }
        ch=fgetc(file);    
    }
    rewind(file);
    while(!feof(file))
    {
        fgets(str,sizeof(str),file);
        if(str[0]=='\n')
        {
            konghang=konghang+1;
            continue;
        }
        for(i=0;str[i]!='\n';i++)
        {
            if(str[i]=='{'||str[i]=='}')
            {
                if(str[i+1]=='\n')
                {
                    konghang=konghang+1;
                    goto ABC;
                }
                else
                {
                    while(str[i+1]==' '||str[i+1]=='\t')
                    {
                        i++;
                        if(str[i+1]=='\n')
                        {
                            konghang=konghang+1;
                            goto ABC;
                        }
                    }
                }
            }
            else
                while(str[i]==' '||str[i]=='\t')
                {
                    i++;
                    if(str[i]=='\n')
                    {
                        konghang=konghang+1;
                        goto ABC;
                    }
                    
                }
        }
        for(i=0;str[i]!='\n'&&str[i]!='\0';i++)
        {
            if(str[i]=='/')
            {
                if(str[i+1]=='/')
                {
                    zhushihang=zhushihang+1;
                    break;
                }
                else
                    break;
            }
            
        }
        ABC:;
    }
    cout<<"空行："<<konghang<<endl;
    cout<<"注释行："<<zhushihang<<endl;
    cout<<"代码行："<<hangshu-konghang-zhushihang+1<<endl;
    rewind(file);
}

int main(int argc, char* argv[])
{
    FILE *file;
    switch(argc)
    {
        case 3:
            file=fopen(argv[2],"r");
            if(file==NULL)
            {
                cout<<"无法打开文件\n"<<endl;
                exit(0);
            }
            if(strcmp(argv[1],"-c")==0)
            {
                CharCount(file);
            }
            else if(strcmp(argv[1],"-w")==0)
            {
                WordCount(file);
            }
            else if(strcmp(argv[1],"-l")==0)
            {
                LineCount(file);
            }
            else if(strcmp(argv[1],"-a")==0)
            {
                Advance(file);
            }
            else
            {
                cout<<"格式有错误\n"<<endl;
            }
            break;
        case 4:
            file=fopen(argv[3],"r");
            if(file==NULL)
            {
                cout<<"无法打开文件\n"<<endl;
                exit(0);
            }
            if(strcmp(argv[1],"-c")==0)
            {
                if(strcmp(argv[2],"-w")==0)
                {
                    CharCount(file);
                    WordCount(file);
                }
                else if(strcmp(argv[2],"-l")==0)
                {
                    CharCount(file);
                    LineCount(file);
                }
                else
                {
                    printf("格式有错误\n");
                }
            }
            else if(strcmp(argv[1],"-w")==0&&
                    strcmp(argv[2],"-l")==0)
            {
                WordCount(file);
                LineCount(file);
            }
            else
            {
                cout<<"格式有错误\n"<<endl;
            }
            break;
        case 5:
            file=fopen(argv[4],"r");
            if(file==NULL)
            {
                printf("无法打开文件\n");
                exit(0);
            }
            // -c -w -l
            if((strcmp(argv[1],"-c")==0)&&
               (strcmp(argv[2],"-w")==0)&&
               (strcmp(argv[3],"-l")==0))
            {
                CharCount(file);
                WordCount(file);
                LineCount(file);
            }
            
            else
            {
                printf("格式有错误\n");
            }
            break;
        case 6:
            file=fopen(argv[5],"r");
            if(file==NULL)
            {
                printf("无法打开文件\n");
                exit(0);
            }
            // -c -w -l -a
            if((strcmp(argv[1],"-c")==0)&&
               (strcmp(argv[2],"-w")==0)&&
               (strcmp(argv[3],"-l")==0)&&
               (strcmp(argv[4],"-a")==0))
            {
                CharCount(file);
                WordCount(file);
                LineCount(file);
                Advance(file);
            }
            else
            {
                printf("格式有错误\n");
            }
            break;
        default:
            {
                cout<<"参数的个数不正确\n"<<endl;
                exit(0);
            }
    }
    return 0;
}

