# 杨锴第二次作业

#### 作业要求
实现一个命令行文本计数统计程序。能正确统计导入的纯英文txt文本中的字符数，单词数，句子数。

#### 使用说明

1.  命令模式： wc.exe [参数] [文件名]
2.  wc.exe -c file.txt 统计字符数
3.  wc.exe -w file.txt 统计单词数
4.  wc.exe -l file.txt 统计句子数
5.  wc.exe -a file.txt 统计代码行、空行、注释行

#### 文件说明

1.  v0.1 空项目
2.  v0.2 项目完成基础功能
3.  v02.png：v02.c测试结果
4.  v0.3 项目完成扩展功能

#### 例程运行及相关结果

1.![图片1](https://gitee.com/wanreno/yang-kais-second-operation/raw/master/1.png)
2.![图片2](https://gitee.com/wanreno/yang-kais-second-operation/raw/master/2.png)
3.![图片3](https://gitee.com/wanreno/yang-kais-second-operation/raw/master/3.png)  
4.![图片4](https://gitee.com/wanreno/yang-kais-second-operation/raw/master/4.png) 
5.![图片5](https://gitee.com/wanreno/yang-kais-second-operation/raw/master/5.png)